<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
//    protected $table = 'vacancies';

    protected $fillable = ['photo','position','company','location'];
}
