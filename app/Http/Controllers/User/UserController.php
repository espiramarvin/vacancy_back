<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use App\Http\Resources\UserResource;
use App\Http\Resources\VacancyResource;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class UserController extends Controller
{
    public function getAll()
    {
        return VacancyResource::collection(User::paginate(10));
    }

    public function editUser($id)
    {
        $data = \request()->validate([
            'name' => 'required',
            'email' => 'required'
        ]);
        $user = User::find($id);
        if (!$user){
            throw new UnprocessableEntityHttpException(' User Not Found');
        }
        $user->update($data);

        return response()->json(['data' => new UserResource($user)]);
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
    }
}
