<?php

namespace App\Http\Controllers\Vacancy;
use App\Http\Controllers\Controller;
use App\Http\Resources\VacancyResource;

use App\Vacancy;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class VacancyController extends Controller
{

    public function add()
    {
        $data = request()->validate([
            'photo' => 'image|nullable|max:1999',
            'position' => 'required',
            'company' => 'required',
            'location' => 'required'
        ]);


        $vacancy = Vacancy::create($data);

        return response()->json($vacancy);

    }

    public function getAll()
    {
        return VacancyResource::collection(Vacancy::paginate(10));
    }

    public function editVacancy($id)
    {
        $data = \request()->validate([
//            'photo' => 'image|nullable|max:1999',
            'position' => 'required',
            'company' => 'required',
            'location' => 'required'
        ]);
        $vacancy = Vacancy::find($id);
        if (!$vacancy){

            throw new UnprocessableEntityHttpException(' Vacancy Not Found');
        }
        $vacancy->update($data);

//        dd($vacancy);
        return response()->json(['data' => new VacancyResource($vacancy)]);
    }

    public function deleteVacancy($id)
    {
        $vacancy = Vacancy::find($id);
        $vacancy->delete();
    }
}
