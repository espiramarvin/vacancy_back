<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class AuthController extends Controller
{
   public function register()
   {
       $data = request()->validate([
           'name' => 'required',
           'email' => 'required|email|unique:users',
           'password' => 'required'
       ]);

       $data['password'] = Hash::make($data['password']);


       $user = User::create($data);

       $user->roles()->attach(2);


       return response()->json($user);

   }

    public function login(Request $request)
    {
        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        if (!Auth::attempt($login)){

            throw new UnprocessableEntityHttpException('Invalid Login Credentials');
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response(['user' => new UserResource(Auth::User()), 'access_token' => $accessToken]);
    }

    public function userDetails()
    {
        $user = request()->user();

        return response()->json(new UserResource($user));
    }
}
