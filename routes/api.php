<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('register','Api\AuthController@register')->name('registerUser');
Route::post('login','Api\AuthController@login');
Route::middleware('auth:api')->get('user_details','Api\AuthController@userDetails');

Route::middleware('auth:api')->post('addVacancy','Vacancy\VacancyController@add')->name('createVacancy');
Route::middleware('auth:api')->put('editVacancy/{id}','Vacancy\VacancyController@editVacancy');
Route::middleware('auth:api')->delete('deleteVacancy/{id}','Vacancy\VacancyController@deleteVacancy');
Route::middleware('auth:api')->get('vacancies','Vacancy\VacancyController@getAll')->name('getVacancy');

Route::middleware('auth:api')->put('edit/{id}','User\UserController@editUser');
Route::middleware('auth:api')->delete('delete/{id}','User\UserController@deleteUser');
Route::middleware('auth:api')->get('users','User\UserController@getAll');

//Route::post('feeds','Posts\PostsController@feeds');

Route::middleware('auth:api')->get('Posts','Posts\PostsController@showFeed');

//Route::feeds();


