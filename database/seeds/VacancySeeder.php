<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class VacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('vacancies')->truncate();
        DB::table('vacancies')->insert([
            [
                'photo' => '',
                'position' => 'Back-end Developer',
                'company' => 'Emploi',
                'location' => 'Nairobi',
            ],
            [
                'photo' => '',
                'position' => 'Business Analyst',
                'company' => 'Safaricom',
                'location' => 'Nairobi',
            ]
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
