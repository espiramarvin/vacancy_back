<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name' => 'espira',
                'email' => 'espira@gmail.com',
                'password' => bcrypt('12345678'),
            ],
            [
                'name' => 'hazard',
                'email' => 'hazard@gmail.com',
                'password' => bcrypt('12345678'),
            ]
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
