<?php


namespace Tests\Unit;


use App\User;

class UserControllerTest extends AbstractHttpTest
{
    public function testStoreUserSuccess()
    {
        $requestBody = [
            'name' => 'marvin',
            'email' => 'marvin@gmail.com',
            'password' => '12345678'
        ];

        $route = route('registerUser');

        $response = $this->ActingAsAdmin()->json('POST',$route,$requestBody);

        $response->assertStatus(200);

        $user = User::where('name','marvin')->first();
        $this->assertEquals($user->name, 'marvin');
    }

    public function testDoesNotStoreUserWithInvalidData()
    {
        $requestBody = [
            'nema' => 'marvin',
            'emil' => 'marvin@gmail.com',
            'paword' => '12345678'
        ];

        $route = route('registerUser');

        $response = $this->ActingAsAdmin()->json('POST',$route,$requestBody);

        $response->assertStatus(422);

    }
}
