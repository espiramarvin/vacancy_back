<?php


namespace Tests\Unit;


use App\User;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use function foo\func;

class AbstractHttpTest extends TestCase
{
    use DatabaseTransactions;

    public function actingAsAdmin()
    {
        return $this->actingAs(User::whereHas('roles', function ($q){
            $q->where('name','admin');
        })->first(),'api'
        );
    }

}

