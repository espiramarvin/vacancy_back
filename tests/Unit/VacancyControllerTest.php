<?php


namespace Tests\Unit;


class VacancyControllerTest extends AbstractHttpTest
{
    public function testReturnsJsonData()
    {
        $route = route('getVacancy');

        $response = $this->ActingAsAdmin()->json('GET',$route);

        $response
            ->assertStatus(200)
            ->assertJson(['data'=>[[
                'photo' => '',
                'position' => 'Back-end Developer',
                'company' => 'Emploi',
                'location' => 'Nairobi',
            ]]]);

    }

}
